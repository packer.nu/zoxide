# Zoxide NuShell Package

Load zoxide into NuShell using [packer.nu][].

**Important Note:** This uses a customized zoxide initialisation.
Issues might come from here and not upstream.

[packer.nu]: https://github.com/jan9103/packer.nu
